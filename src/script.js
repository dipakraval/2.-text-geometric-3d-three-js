import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader'

import * as dat from 'dat.gui'

const canvas = document.querySelector(".webgl");

/**
 * Texture Loader
 */
const loader = new THREE.TextureLoader();
const textureLoader = loader.load('/textures/Material_1912.jpg');


/**
 * Scene
 */
const scene = new THREE.Scene();
scene.background = new THREE.Color(0x000000);







/**
 * 
 * Text Geometry
 */
const fontLoader = new THREE.FontLoader();
fontLoader.load("fonts/helvetiker_regular.typeface.json",
	function(font){
		const text = new THREE.TextGeometry(
			"Dipak Raval",
			{
				font: font,
				size: 80,
				curveSegments: 100,
				bevelEnabled: true,
				bevelThickness: 0,
				bevelSize: 1,
				bevelOffset: 1,
				bevelSegments: 10
			}
		);

		const textMaterial = new THREE.MeshNormalMaterial();
		const textMesh = new THREE.Mesh(text,textMaterial);

		// text.computeBoundingBox();
		// text.translate(
		// 	- text.boundingBox.max.x * 0.5,
		// 	- text.boundingBox.max.y * 0.5,
		// 	- text.boundingBox.max.z * 0.5,
		// );

		text.center();
		scene.add(textMesh);

		
				

	}
);

const group = new THREE.Group();

var countTorus = 0;
var removeTorus = 0;
for(let i=0 ; i<450 ; i++){
	const geometry = new THREE.TorusGeometry( 50, 30, 16, 100 );
	const material = new THREE.MeshNormalMaterial( );
	const torus = new THREE.Mesh( geometry, material );

	torus.position.x = ((Math.random() - 0.5 ) * 999) * 1.6;
	torus.position.y = ((Math.random() - 0.5 ) * 999) * 1.6;
	torus.position.z = ((Math.random() - 0.5 ) * 999) * 1.6;

	torus.rotation.x = (Math.random() - 0.5 ) * 999;
	torus.rotation.y = (Math.random() - 0.5 ) * 999;

	const scale = Math.random();
	torus.scale.set(scale,scale,scale);
	countTorus++;
	group.add(torus)
}

scene.add(group);
/**
 * Scrolling event
 */

 window.addEventListener("wheel", function(event) {
	
	
	if (event.deltaY < 0)
	{
		// Up Direction
		posotionScrollDown();
	}
	else if (event.deltaY > 0)
	{
		// Down Direction
		posotionScrollUp();
	}
}, true);

/**
 * Camera
 */
const size = {
    width: window.innerWidth,
    height: window.innerHeight
}
const camera = new THREE.PerspectiveCamera(74,size.width / size.height,1,1000);
camera.position.z = 400;

scene.add( camera );





/**
 * Light
 */
// const light = new THREE.AmbientLight( 0xffffff,0.5 );
// scene.add(light);



/**
 * OrbitControls
 */ 
const control = new OrbitControls(camera, canvas);
control.enableZoom = false;
control.enableDamping = true;
control.minPolarAngle = 0; // radians
control.maxPolarAngle = Math.PI; // radians


/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(size.width,size.height);


/**
 * Resize Event
 */
window.addEventListener( 'resize', onWindowResize, false );
		
function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize( window.innerWidth, window.innerHeight );

}

const mq = window.matchMedia( "(min-width: 768px)" );
if (mq.matches) {
} else {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	camera.position.z = 860;
	renderer.setSize( window.innerWidth, window.innerHeight );
}

/**
 * Animation
 */
const tick = () => {

    control.update();

	for(let i=0 ; i<450 ; i++){
		group.children[i].rotation.x += 0.010;
		group.children[i].rotation.y += 0.010;
	}
	
    renderer.render(scene,camera);
    window.requestAnimationFrame(tick);

}
tick();

function onWheel(){

}

/**
 * Function change position
 */
function posotionScrollDown(event){
	group.position.z -= 30;
	// Add Torus 
	for(let i=0 ; i<30 ; i++){
		
		const geometry = new THREE.TorusGeometry( 50, 30, 16, 100 );
		const material = new THREE.MeshNormalMaterial( );
		const torus = new THREE.Mesh( geometry, material );
		
		
		torus.position.x = ((Math.random() - 0.5 ) * 2000)  + group.position.y;
		torus.position.y = ((Math.random() - 0.5 ) * 999)  + group.position.y;
		torus.position.z = -(group.position.z - ((Math.random() - 0.5 ) * 2000));
		
		torus.rotation.x = (Math.random() - 0.5 ) * 999;
		torus.rotation.y = (Math.random() - 0.5 ) * 999;
		
		const scale = Math.random();
		torus.scale.set(scale,scale,scale);
		countTorus++;
		group.add(torus)
	}


	// Remove Torus
	var removeCount = removeTorus + 30;
	var removeI = 0;
	for(let i=removeTorus ; i<removeCount ; i++){
		
		group.remove(group.children[removeI]);
		removeI++;
		removeTorus++;
	}
}


function posotionScrollUp(event){
	group.position.z += 30;
		
	// Add Torus 
	for(let i=0 ; i<30 ; i++){
		
		const geometry = new THREE.TorusGeometry( 50, 30, 16, 100 );
		const material = new THREE.MeshNormalMaterial( );
		const torus = new THREE.Mesh( geometry, material );
		
		
		torus.position.x = ((Math.random() - 0.5 ) * 2000)  + group.position.y;
		torus.position.y = ((Math.random() - 0.5 ) * 999)  + group.position.y;
		torus.position.z = -(group.position.z + ((Math.random() - 0.5 ) * 2000));
		
		torus.rotation.x = (Math.random() - 0.5 ) * 999;
		torus.rotation.y = (Math.random() - 0.5 ) * 999;
		
		const scale = Math.random();
		torus.scale.set(scale,scale,scale);
		countTorus++;
		group.add(torus)
	}


	// Remove Torus
	var removeCount = removeTorus + 30;
	var removeI = 0;
	for(let i=removeTorus ; i<removeCount ; i++){
		
		group.remove(group.children[removeI]);

		removeI++;
		removeTorus++;
	}
}


// /**
//  * On button down click
//  */
//  var tmp = 0;
//  $(".btn").click(function(){
	 
// 	 if(tmp == 0){
// 		 control.enabled = false;
// 		 control.update();
// 		 createJoyStick();
// 		 tmp = 1;
// 	 }else{
// 		 control.enabled = true;
// 		 control.update();
 
// 		 tmp = 0;
// 	 }
//  });

// /**
//  * Joy Sticks
//  */

// function createJoyStick(){
// 	var joystick = new VirtualJoystick({
// 		container	: document.body,
// 		strokeStyle	: 'cyan',
// 		limitStickTravel: true,
// 		stickRadius	: 120	
// 	})
// 	console.log(joystick);
	
	
// 	setInterval(function(){	
// 		joystick.up() ? posotionScrollUp() : '';
// 	}, 1/30 * 1000);
// }
